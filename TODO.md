## VIMRC
- [x] Add save on refocus
- [x] Add saving previous line position
- [x] Change color scheme
- [x] Ignore case, smartcase, hightlight search, incremental search
- [x] Smarttab, expandtab, autoindent, smartindent
- [x] Tabstop = 4
- [x] Disable mouse use in terminal

